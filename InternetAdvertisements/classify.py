# Naive Bayes Classification on Internet Advertisements dataset of UCI Machine Learning Repository.

# Rows with missing features removed, considering all attributes - Accuracy 80%

# Missing features filled with mean of that attribute, considering all attributes - Accuracy 78.1%

# Missing features filled with mean of that attribute, using SelectKBest to select 3 most important attributes - Accuracy 94%

# Found accuracies for all values of k (for SelectKBest) to find the optimum number of important features to use. - Returned k = 510 with accuracy 97.1942%

# Found 510 attributes most important in deciding whether the page is an advertisement or not. (Results in file: main_attributes.txt)

# Switched to KNeighborsClassifier: Accuracy - 98.4751448612%

# Redid selection of k for SelectKBest using KNeighborsClassifier to get accuracy-
# 423 features.
# Accuracy 98.5361390668%
# 423 attributes written in main_attributes.txt

import csv
from sklearn.naive_bayes import GaussianNB
import pandas as pd
import numpy as np
from sklearn.preprocessing import Imputer
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.neighbors import KNeighborsClassifier

classes = {'ad.':0, 'nonad.':1}

def loadDataset(filename):
	dataset = pd.read_csv(filename, sep = ',', skipinitialspace = True, header = None).replace(to_replace = '?', value = np.nan)
	imputer = Imputer(missing_values = np.nan, strategy = 'mean', axis = 0)
	classSet = dataset.loc[:, len(dataset.columns)-1].replace(to_replace = 'ad.', value = classes['ad.']).replace(to_replace = 'nonad.', value = classes['nonad.'])
	dataset = dataset.loc[:, :len(dataset.columns)-2]
	dataset = imputer.fit_transform(dataset)
	return dataset, classSet

def getAccuracy(testSet, predictions):
	correct = 0
	for x in range(len(testSet)):
		if testSet[x] == predictions[x]:
			correct += 1
	return (correct/float(len(testSet)))*100.0

#to remove rows with missing values.
def cleanDataset(filename):
	with open(filename, 'rb') as f:
		lines = csv.reader(f)
		dataset = list(lines)
		cleanDataset = []
		classSet = []
		for instance in dataset:
			found  =0
			for i in instance:
				if '?' in i:
					found = 1
			if found == 0:
				cleanDataset.append(instance)
		for instance in cleanDataset:
			for i in range(len(instance)-1):
				instance[i] = float(instance[i])
			classSet.append(classes[instance[-1]])
			del instance[-1]
	return cleanDataset, classSet

def applyNB(dataset, classSet):
	gnb = GaussianNB()
	y_pred = gnb.fit(dataset, classSet).predict(dataset)
	accuracy = getAccuracy(classSet, y_pred)
	return accuracy, y_pred

def findBestK(dataset, classSet, classifier):
	accuracymax = 0
	kbest = 0
	for k in range(1, 1378):
		if k%100 == 0:
			print 'On ', k
		sel = SelectKBest(chi2, k = k)
		checkdataset = sel.fit_transform(dataset, classSet)
		accuracy, y_pred = classifier(checkdataset, classSet)
		if accuracy > accuracymax:
			accuracymax = accuracy
			kbest = k
	return kbest, accuracymax
def attributes(attr_filename):
	attrs = []
	with open(attr_filename, 'r') as f:
		for line in f:
			attrs.append(line)
	return attrs

def mainAttributes(attributes, support):
	mainattrs = []
	for index in support:
		mainattrs.append(attributes[index])
	return mainattrs

def applyKNeighbors(dataset, classSet):
	knc = KNeighborsClassifier(n_neighbors = 2)
	y_pred = knc.fit(dataset, classSet).predict(dataset)
	accuracy = getAccuracy(classSet, y_pred)
	return accuracy, y_pred

def main():
	filename = "Data/ad.data"
	attr_filename = "Data/ad.attributes.txt"
	dataset, classSet = loadDataset(filename)
	#dataset, classSet = cleanDataset(filename)
	print 'Loaded ', len(dataset), ' rows'
	#kbest, accuracy = findBestK(dataset, classSet, applyKNeighbors)
	sel = SelectKBest(chi2, k = 423)
	dataset = sel.fit_transform(dataset, classSet)
	accuracy, y_pred = applyKNeighbors(dataset, classSet)
	print 'Accuracy ', accuracy
	support = sel.get_support(True)
	attrs = attributes(attr_filename)
	mainattrs = mainAttributes(attrs, support)
	with open("main_attributes.txt", 'w') as f:
		for attr in mainattrs:
			f.write(attr)
main()