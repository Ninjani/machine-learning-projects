Internet Advertisements Data Set 
--------------------------------
Abstract: This dataset represents a set of possible advertisements on Internet pages.



Data Set Information:
---------------------
This dataset represents a set of possible advertisements on Internet pages. The features encode the geometry of the image (if available) as well as phrases occuring in the URL, the image's URL and alt text, the anchor text, and words occuring near the anchor text. The task is to predict whether an image is an advertisement ("ad") or not ("nonad").



Attribute Information:
----------------------
(3 continous; others binary; this is the "STANDARD encoding" mentioned in the [Kushmerick, 99].) 

One or more of the three continous features are missing in 28% of the instances; missing values should be interpreted as "unknown".


RESULTS:
--------
# Found 423 attributes out of 1558 most important in deciding whether the page is an advertisement or not. (Results in file: main_attributes.txt)

Classified using KNeighborsClassifier: Accuracy - 98.5361390668%



Source:
-------
UCL Machine Learning Repository - http://archive.ics.uci.edu/ml/datasets/Internet+Advertisements
Creator & donor: 

Nicholas Kushmerick <nick '@' ucd.ie>



