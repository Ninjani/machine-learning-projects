#Used 77 protein features to separately predict each of genotype, treatment and behavior, with missing values filled by 
#	1. mean - Genotype accuracy  78.8888888889, Treatment accuracy  67.962962963, Behavior accuracy  97.4074074074
#	2.median - Genotype accuracy  78.6111111111, Treatment accuracy  67.962962963, Behavior accuracy  97.5
#	3.most_frequent - Genotype accuracy  77.5, Treatment accuracy  68.1481481481, Behavior accuracy  97.1296296296

#Used SelectKBest to find optimum number of features to use for each, with missing values filled by:
#	1.mean -
#		Genotype accuracy  79.9074074074 	Number of features  13
#		Treatment accuracy  68.6111111111 	Number of features  58
#		Behavior accuracy  99.0740740741 	Number of features  4

#	2.median -
#		Genotype accuracy  79.8148148148 	Number of features  13
#		Treatment accuracy  68.5185185185 	Number of features  58
#		Behavior accuracy  99.1666666667 	Number of features  5

#	3.most_frequent - 
#		Genotype accuracy  81.2962962963 	Number of features  14
#		Treatment accuracy  69.7222222222 	Number of features  62
#		Behavior accuracy  98.7962962963 	Number of features  4

#From this data, we imply
#1. Genotype classfication - fill by most_frequent, accuracy 81.2962962963%,  k = 14
#2. Treatment classification - fill by most_frequent, accuracy 69.7222222222%, k = 62
#3. Behavior classification - fill by median, accuracy 99.1666666667%, k = 5 

#Now adding the other two classes while training to predict the third class
# Genotype accuracy  81.2962962963 	Number of features  14
# Treatment accuracy  70.0 	Number of features  63
# Behavior accuracy  99.1666666667 	Number of features  5
# Only treatment accuracy is improved by adding Genotype and Behavior to the attributes under consideration 

#Stored names of proteins important in discriminating Genotype (proteins_genotype.txt), Treatment (proteins_treatment.txt) and Behavior (proteins_behavior.txt)

#Used KNeighborsClassfifier instead of Naive Bayes:
# Genotype accuracy  100.0
# Treatment accuracy  99.8148148148
# Behavior accuracy  100.0

#Redid selection of k for SelectKBest using KNeighborsClassifier to get accuracy-
#	Genotype accuracy  100.0 	Number of features  14
#	Treatment accuracy  100.0 	Number of features  48
#	Behavior accuracy  100.0 	Number of features  4
# All are only dependent on proteins,  not on genotype, treatment or behavior


import csv
from sklearn.naive_bayes import GaussianNB
import pandas as pd
import numpy as np
from sklearn.preprocessing import Imputer
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_regression
from sklearn.neighbors import KNeighborsClassifier
import os

genotypes = {'Control':0, 'Ts65Dn':1}
treatments = {'Memantine':0, 'Saline':1}
behaviors = {'C/S':0, 'S/C':1}

def loadDataset(filename):
	dataset = pd.read_excel(filename, sep = ',').fillna(np.nan)
	imputermostfreq = Imputer(missing_values = np.nan, strategy = 'most_frequent', axis = 0)
	imputermed = Imputer(missing_values = np.nan, strategy = 'median', axis = 0)
	genotypeSet = dataset["Genotype"].replace(to_replace = 'Control', value = genotypes['Control']).replace(to_replace = 'Ts65Dn', value = genotypes['Ts65Dn'])
	treatmentSet = dataset["Treatment"].replace(to_replace = 'Memantine', value = treatments['Memantine']).replace(to_replace = 'Saline', value = treatments['Saline'])
	behaviorSet = dataset["Behavior"].replace(to_replace = 'C/S', value = behaviors['C/S']).replace(to_replace = 'S/C', value = behaviors['S/C'])
	IDs = dataset["MouseID"]
	proteinNames = list(dataset.columns.values)
	del proteinNames[0]
	del proteinNames[80]
	del proteinNames[79]
	del proteinNames[78]
	del proteinNames[77]
	proteinsGenotype = pd.DataFrame(imputermostfreq.fit_transform(dataset.ix[:, 1:77]))
	proteinsTreatment = pd.DataFrame(imputermostfreq.fit_transform(dataset.ix[:, 1:77]))
	proteinsBehavior = pd.DataFrame(imputermed.fit_transform(dataset.ix[:, 1:77]))
	return IDs, proteinsGenotype, proteinsTreatment, proteinsBehavior, genotypeSet, treatmentSet, behaviorSet, proteinNames

def getAccuracy(testSet, predictions):
	correct = 0
	for x in range(len(testSet)):
		if testSet[x] == predictions[x]:
			correct += 1
	return (correct/float(len(testSet)))*100.0

def applyNB(dataset, classSet):
	gnb = GaussianNB()
	y_pred = gnb.fit(dataset, classSet).predict(dataset)
	accuracy = getAccuracy(classSet, y_pred)
	return accuracy, y_pred

def applyKNeighbors(dataset, classSet):
	knc = KNeighborsClassifier(n_neighbors = 2)
	y_pred = knc.fit(dataset, classSet).predict(dataset)
	accuracy = getAccuracy(classSet, y_pred)
	return accuracy, y_pred

def findBestK(dataset, classSet, classifier):
	accuracymax = 0
	kbest = 0,
	for k in range(2, 76):
		sel = SelectKBest(f_regression, k = k)
		checkdataset = sel.fit_transform(dataset, classSet)
		accuracy, y_pred = classifier(checkdataset, classSet)
		if accuracy > accuracymax:
			accuracymax = accuracy
			kbest = k
	return kbest, accuracymax

def mainAttributes(attributes, support):
	mainattrs = []
	for index in support:
		mainattrs.append(attributes[index])
	return mainattrs

def main():
	filename = 'file://localhost/' + os.getcwd() + '/Data_Cortex_Nuclear.xls'
	IDs, proteinsGenotype, proteinsTreatment, proteinsBehavior, genotypeSet, treatmentSet, behaviorSet, proteinNames = loadDataset(filename)
	print 'Loaded ', len(genotypeSet), ' rows'
	#kbestGenotype, accuracyGenotype = findBestK(proteinsGenotype, genotypeSet, applyKNeighbors)
	#kbestTreatment, accuracyTreatment = findBestK(proteinsTreatment, treatmentSet, applyKNeighbors)
	#kbestBehavior, accuracyBehavior = findBestK(proteinsBehavior, behaviorSet, applyKNeighbors)
	kbestGenotype = 14
	kbestTreatment = 48
	kbestBehavior = 4
	selGenotype = SelectKBest(f_regression, k = kbestGenotype)
	selTreatment = SelectKBest(f_regression, k = kbestTreatment)
	selBehavior = SelectKBest(f_regression, k = kbestBehavior)
	accuracyGenotype, genotype_pred = applyKNeighbors(selGenotype.fit_transform(proteinsGenotype, genotypeSet), genotypeSet)
	accuracyTreatment, treatment_pred = applyKNeighbors(selTreatment.fit_transform(proteinsTreatment, treatmentSet), treatmentSet)
	accuracyBehavior, behavior_pred = applyKNeighbors(selBehavior.fit_transform(proteinsBehavior, behaviorSet), behaviorSet)
	print 'Genotype accuracy ', accuracyGenotype
	print 'Treatment accuracy ', accuracyTreatment
	print 'Behavior accuracy ', accuracyBehavior

	proteins = []
	for protein in proteinNames:
		proteins.append(str(protein))
	attrsGenotype = mainAttributes(proteins, selGenotype.get_support(True))
	attrsTreatment = mainAttributes(proteins, selTreatment.get_support(True))
	attrsBehavior = mainAttributes(proteins, selBehavior.get_support(True))
	with open("proteins_genotype.txt", 'w') as f:
		for attr in attrsGenotype:
			f.write(attr + '\n')

	with open("proteins_treatment.txt", 'w') as f:
		for attr in attrsTreatment:
			f.write(attr + '\n')

	with open("proteins_behavior.txt", 'w') as f:
		for attr in attrsBehavior:
			f.write(attr + '\n')

main()